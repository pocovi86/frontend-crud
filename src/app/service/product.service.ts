import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Product} from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  productUrl = 'http://localhost:8080/api/';

  constructor(private httpClient: HttpClient) {
  }

  // metodos del backend
  public list(): Observable<Product[]> {
    return this.httpClient.get<Product[]>(this.productUrl + 'listProduct');
  }

  public detail(id: number): Observable<Product> {
    return this.httpClient.get<Product>(this.productUrl + `detail/${id}`);
  }

  public detailName(name: string): Observable<Product> {
    return this.httpClient.get<Product>(this.productUrl + `detailName/${name}`);
  }

  public save(product: Product): Observable<any>{
    return this.httpClient.post<any>(this.productUrl + 'create', product);
  }

  public update(id: number, prodcut: Product): Observable<any>{
    return this.httpClient.put<any>(this.productUrl + `update/${id}`, prodcut);
  }

  public delete(id: number): Observable<any>{
    return this.httpClient.delete<any>(this.productUrl + `delete/${id}`);
  }
}
